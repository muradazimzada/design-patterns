package com.example.designpatterns.patterns.behavorial.observer;

import java.io.File;

public interface EventListener {
    void update(String eventType, File file);
}