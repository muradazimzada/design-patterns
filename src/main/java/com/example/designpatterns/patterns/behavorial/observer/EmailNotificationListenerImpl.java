package com.example.designpatterns.patterns.behavorial.observer;

import java.io.File;

public class EmailNotificationListenerImpl implements EventListener {
    private String email;

    public EmailNotificationListenerImpl(String email) {
        this.email = email;
    }


    @Override
    public void update(String eventType, File file) {
        System.out.println("Email to " + email + ": Someone has performed " + eventType + " operation with the following file: " + file.getName());

    }
}