package com.example.designpatterns.patterns.creational.factory;

public class HtmlButton implements Button{
    @Override
    public void render() {
        System.out.println("HTML render");
    }

    @Override
    public void onClick() {
        System.out.println("Html onClick");
    }
}
