package com.example.designpatterns.patterns.creational.factory;

public class HtmlDialogImpl extends DialogFactory {
    @Override
    public Button createButton() {
        return new HtmlButton();
    }
}
