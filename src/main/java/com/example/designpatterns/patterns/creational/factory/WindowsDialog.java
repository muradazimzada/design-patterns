package com.example.designpatterns.patterns.creational.factory;

public class WindowsDialog extends DialogFactory {

    @Override
    public Button createButton() {
        return new WindowsButton();
    }
}