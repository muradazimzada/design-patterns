package com.example.designpatterns.patterns.creational.factory;

public abstract class DialogFactory {

    public void renderWindow() {
        // ... other code ...

        Button okButton = createButton();
        okButton.render();
    }

    public abstract Button createButton();
}
