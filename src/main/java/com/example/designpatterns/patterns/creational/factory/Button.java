package com.example.designpatterns.patterns.creational.factory;

public interface Button {
    void render();
    void onClick();
}
