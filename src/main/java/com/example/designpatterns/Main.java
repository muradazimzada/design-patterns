package com.example.designpatterns;

import com.example.designpatterns.patterns.creational.factory.DialogFactory;
import com.example.designpatterns.patterns.creational.factory.HtmlDialogImpl;
import com.example.designpatterns.patterns.creational.factory.WindowsDialog;
import com.example.designpatterns.patterns.creational.singleton.Singleton;
import com.example.designpatterns.patterns.creational.prototype.Circle;
import com.example.designpatterns.patterns.creational.prototype.Rectangle;
import com.example.designpatterns.patterns.creational.prototype.Shape;
import com.example.designpatterns.patterns.structural.adapter.SocketAdapter;
import com.example.designpatterns.patterns.structural.adapter.SocketAdepterImpl;
import com.example.designpatterns.patterns.structural.adapter.Volt;
import com.example.designpatterns.patterns.behavorial.observer.Editor;
import com.example.designpatterns.patterns.behavorial.observer.EmailNotificationListenerImpl;
import com.example.designpatterns.patterns.behavorial.observer.LogOpenListener;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static DialogFactory dialog;


    public static void main(String[] args) {

        testObserver();

        //testClassAdapter();
        //testPrototype();

        }

public static void testObserver()
{
    Editor editor = new Editor();
    editor.events.subscribe("open", new LogOpenListener("file.txt"));
    editor.events.subscribe("save", new EmailNotificationListenerImpl("admin@example.com"));

    try {
        editor.openFile("test.txt");
        editor.saveFile();
    } catch (Exception e) {
        e.printStackTrace();
    }
}

       public void testFactory() {
            configure();
            runBusinessLogic();
        }

        static void testPrototype() {

            List<Shape> shapes = new ArrayList<>();
            List<Shape> shapesCopy = new ArrayList<>();

            Circle circle = new Circle();
            circle.x = 10;
            circle.y = 20;
            circle.radius = 15;
            circle.color = "red";
            shapes.add(circle);

            Circle anotherCircle = (Circle) circle.clone();
            shapes.add(anotherCircle);

            Rectangle rectangle = new Rectangle();
            rectangle.width = 10;
            rectangle.height = 20;
            rectangle.color = "blue";
            shapes.add(rectangle);

            cloneAndCompare(shapes, shapesCopy);
        }
    private static void cloneAndCompare(List<Shape> shapes, List<Shape> shapesCopy) {
        for (Shape shape : shapes) {
            shapesCopy.add(shape.clone());
        }

        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) != shapesCopy.get(i)) {
                System.out.println(i + ": Shapes are different objects (yay!)");
                if (shapes.get(i).equals(shapesCopy.get(i))) {
                    System.out.println(i + ": And they are identical (yay!)");
                } else {
                    System.out.println(i + ": But they are not identical (booo!)");
                }
            } else {
                System.out.println(i + ": Shape objects are the same (booo!)");
            }
        }
    }
        void testSingleton() {

            System.out.println("If you see the same value, then singleton was reused (yay!)" + "\n" +
                    "If you see different values, then 2 singletons were created (booo!!)" + "\n\n" +
                    "RESULT:" + "\n");
            Singleton singleton = Singleton.getInstance("FOO");
            Singleton anotherSingleton = Singleton.getInstance("BAR");
            System.out.println(singleton.value);
            System.out.println(anotherSingleton.value);


        }        /**
         * The concrete factory is usually chosen depending on configuration or
         * environment options.
         */
        static void configure() {
            if (System.getProperty("os.name").equals("Windows 11")) {
                dialog = new WindowsDialog();
            } else {
                dialog = new HtmlDialogImpl();
            }
        }

    private static void testClassAdapter() {
        SocketAdapter sockAdapter = new SocketAdepterImpl();
        Volt v3 = sockAdapter.get3Volt();
        Volt v12 = sockAdapter.get12Volt();
        Volt v120 = sockAdapter.get12Volt();
        System.out.println("v3 volts using Class Adapter="+v3.getVolts());
        System.out.println("v12 volts using Class Adapter="+v12.getVolts());
        System.out.println("v120 volts using Class Adapter="+v120.getVolts());
    }
        /**
         * All of the client code should work with factories and products through
         * abstract interfaces. This way it does not care which factory it works
         * with and what kind of product it returns.
         */
        static void runBusinessLogic() {
            dialog.renderWindow();
        }
    }
